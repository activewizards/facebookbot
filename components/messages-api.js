'use strict';

var request = require('request');

function MessagesApi(config) {
    var self = {};

    self.sendMessage = function(recipientId, msg) {
        if (config.DEBUG) {
            console.log('sending msg to fb ...', JSON.stringify(msg));
            // return;
        }

        request({
            url: 'https://graph.facebook.com/v3.2/me/messages',
            qs: {access_token: config.FB_ACCESS_TOKEN},
            method: 'POST',
            json: {
                recipient: {id: recipientId},
                message: msg,
            }
        }, function(error, response, body) {
            if (error) {
                console.log('Error sending message: ', error);
            } else if (response.body.error) {
                console.log('Error: ', response.body.error);
            }
        });
    };

    self.sendCurrentWeather = function(recipientId, forecast) {
        var weather = forecast.weather[0].description;
        var temp = "Temp: " + forecast.main.temp + "F";
        var pressure = "Pressure: " + forecast.main.pressure + "hPa";
        var wind = "Wind: " + forecast.wind.speed + "m/s";
        var image_url = "http://openweathermap.org/img/w/" + forecast.weather[0].icon + ".png";

        var payload = {
            template_type: "generic",
            elements: [{
                title: forecast.sys.country + " " + forecast.name,
                subtitle: weather + " " + temp + " " + pressure + " " + wind,
                image_url: image_url
            }]
        };
        var msg = {
            attachment: {
                type: "template",
                payload: payload
            }
        };
        self.sendMessage(recipientId, msg);
    };

    return self;  
}

module.exports = MessagesApi;
