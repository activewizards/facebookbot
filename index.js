'use strict';

var config = require('./config');
var FbApiWrapper = require('./components/messages-api')(config);
var WitSessions = require('./components/wit-sessions');
var OpenWeatherApi = require('./components/openweather-api')(config);

var express = require('express');
var bodyParser = require('body-parser');
var util = require('util');

var app = express();

const {Wit, log} = require('node-wit');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.listen(config.PORT);

// server frontpage
app.get('/', function (req, res) {
    console.log('/');
    res.send('This is Eliza Bot Server');
});

// Setting up bot
const wit = new Wit({
    accessToken: config.WIT_ACCESS_TOKEN,
    // actions,
    logger: new log.Logger(log.INFO)
});

// endpoint for local testing, msg send in q parameter
app.get('/wit', function (req, res) {
    const sender = '1421421421';
    const text = req.query.q;
    processUserInput(sender, text);
    res.sendStatus(200);

});

// facebook Webhook
app.get('/webhook', function (req, res) {
    if (req.query['hub.verify_token'] === 'testbot_verify_token') {
        res.send(req.query['hub.challenge']);
    } else {
        res.send('Invalid verify token');
    }
});

// handler receiving messages
app.post('/webhook', function (req, res) {
    var events = req.body.entry[0].messaging;
    for (var i = 0; i < events.length; i++) {
        var event = events[i];
        // handle quick_reply buttons
        if (event.message && event.message.text &&
            event.message.quick_reply) {
            // call openweather api directly
            handleQuickReply(event.sender, event.message);
        }
        // precess to wit bot
        else if (event.message && event.message.text) {
            processUserInput(
                event.sender.id,
                event.message.text
            );
        }
    }
    res.sendStatus(200);
});

function processUserInput(sender, text) {
    const sessionId = WitSessions.findOrCreateSession(sender);
    wit.message(text, WitSessions.getContext(sessionId))
        .then(data => {
            let entities = data.entities;
            const intent = firstEntity(entities, 'intent');
            const fbId = WitSessions.getFbId(sessionId);
            let location = entities.location;
            if (!intent && (!location || !location[0] || !location[0].resolved)) {
                const response = {
                    text: "Sorry, but I don't understand you." +
                        "Please specify location, and I will give" +
                        "you current weather in that place"
                };
                return FbApiWrapper.sendMessage(
                    fbId, response
                );
            } else if (location && location[0] && location[0].resolved) {
                return OpenWeatherApi.getCurrentWeather({forecast: location[0].value}, (err, forecast) => {
                    FbApiWrapper.sendCurrentWeather(
                        fbId, forecast
                    )
                });
            }

            switch (intent.value) {
                case 'weather':
                    if (!location) {
                        return FbApiWrapper.sendMessage(
                            fbId, 'Specify the location in your request please.'
                        )
                    }
                    OpenWeatherApi.getCurrentWeather({forecast: location[0].value}, (err, forecast) => {
                        FbApiWrapper.sendCurrentWeather(
                            fbId, forecast
                        );
                    });
                    break;
                case 'greetings':
                    FbApiWrapper.sendMessage(
                        fbId, getGreetingMsg()
                    );
                    break;
                default:
                    console.log(`Default case for: ${intent.value}`);
                    return 'Can\'t find intent';
            }
        })
        .catch(err => {
            console.log('Oops! Got an error: ', err.stack || err);
        })
}

function firstEntity(entities, name) {
    return entities &&
        entities[name] &&
        Array.isArray(entities[name]) &&
        entities[name] &&
        entities[name][0];
}

function getGreetingMsg() {
    let items = [
        "Hi there! I am a weather bot.",
        "Welcome to the weather bot. Give me a location and I'll send forecast.",
        "Hi, welcome to this bot."
    ];
    let default_options = 'Pick an option below to get going';

    let text = util.format(
        '%s %s:',
        items[Math.floor(Math.random()*items.length)], default_options
    );
    let response = {
        text: text,
        quick_replies: [
            {
                content_type: "text",
                title: "London",
                payload: "PAYLOAD_FOR_PICKING_LONDON"
            },
            {
                content_type: "text",
                title: "Paris",
                payload: "PAYLOAD_FOR_PICKING_PARIS"
            },
            {
                content_type: "text",
                title: "New York",
                payload: "PAYLOAD_FOR_PICKING_NEW_YORK"
            }
        ]
    };

    return response;
}

function handleQuickReply(sender, message) {
    let location = { forecast: message.text };

    return OpenWeatherApi.getCurrentWeather(location, (err, forecast) => {
        FbApiWrapper.sendCurrentWeather(
            sender.id, forecast
        );
    });
}
