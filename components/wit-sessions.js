'use strict';

// This will contain all user sessions.
// Each session has an entry:
// sessionId -> {fbid: facebookUserId, context: sessionState}

function WitSessions() {  
    var self = {};

    self.findOrCreateSession = function(fbid) {
        let sessionId;
        // Let's see if we already have a session for the user fbid
        Object.keys(self._sessions).forEach(k => {
            if (self._sessions[k].fbid === fbid) {
                // Yep, got it!
                sessionId = k;
            }
        });
        if (!sessionId) {
            // No session found for user fbid, let's create a new one
            sessionId = new Date().toISOString();
            self._sessions[sessionId] = {fbid: fbid, context: {}};
        }
        return sessionId;
    };

    self.getContext = function(sessionId) {
        return self._sessions[sessionId].context;
    };

    self.setContext = function(sessionId, context) {
        self._sessions[sessionId].context = context;
    };

    self.getFbId = function(sessionId) {
        return self._sessions[sessionId].fbid;
    };

    self._sessions = {};

    return self;
}

module.exports = WitSessions();
