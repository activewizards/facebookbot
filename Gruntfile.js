var config = require('./config');

module.exports = function(grunt) {
    "use strict";

    // Project configuration.
    grunt.initConfig({
        watch: {
            options: {
                livereload: true
            },
            express: {
                files:  [ 'config.js', 'index.js', 'components/*.js' ],
                tasks:  [ 'express:dev' ],
                options: {
                    spawn: false
                }
            }
        },
        express: {
            options: {
                port: config.PORT,
            },
            dev: {
                options: {
                    script: 'index.js'
                }
            }
        },
        jshint: {
            options: {
                node: true,
                esversion: 6
            },
            all: [
                    'Gruntfile.js', 'config.js',
                    'index.js', 'components/*.js'
                ]
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express-server');

    grunt.registerTask('server', [ 'express:dev', 'watch' ]);
    grunt.registerTask('hint', ['jshint:all']);
};
