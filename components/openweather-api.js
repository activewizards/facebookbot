'use strict';

var request = require('request');

function OpenWeatherApi(config) {
    var self = {};

    self.getCurrentWeather = function(location, callback) {
        request({
            url: 'http://api.openweathermap.org/data/2.5/weather',
            qs: {
                appid: config.OPENWEATHER_TOKEN,
                q: location.forecast,
                mode: 'json'
            },
            method: 'GET',
        },(error, response, body) => {
            if (error) {
                console.log('Error sending message: ', error);
                callback(error, undefined);
            } else if (body.error) {
                console.log('Error: ', body.error);
                callback(body.error, undefined);
            }
            else {
                callback(undefined, JSON.parse(body));
            }
        });
    };

    return self;  
}

module.exports = OpenWeatherApi;
